<?php

namespace App\Factories;

use App\Models\Address as ModelAddress;
use App\Helpers\Utils;

class Address extends AbstractFactory
{
    public static function create($data, $address = null)
    {
        if (!$address) {
            $address = new ModelAddress();
        }

        if (!empty($data['zipcode'])) {
            $data['zipcode'] = Utils::getJustDigits($data['zipcode']);
        }
        
        static::populateObject($data, $address, 'zipcode', 'zipcode');
        static::populateObject($data, $address, 'city', 'city');
        static::populateObject($data, $address, 'uf', 'uf');
        static::populateObject($data, $address, 'state', 'street');
        static::populateObject($data, $address, 'number', 'number');
        static::populateObject($data, $address, 'street', 'street');
        static::populateObject($data, $address, 'neighborhood', 'neighborhood');
        static::populateObject($data, $address, 'reference', 'reference');
        static::populateObject($data, $address, 'complement', 'complement');

        return $address;
    }
}
<?php

namespace App\Factories;

abstract class AbstractFactory
{
    /**
     * @param array $data
     * @param mixed $object
     * @param string $nameValue
     * @param string $propName
     */
    protected static function populateObject($data, $object, $nameValue, $propName)
    {
        if (isset($data[$nameValue])) {
            $object->$propName = $data[$nameValue];
        }
    }
}

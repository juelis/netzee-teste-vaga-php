<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $image
 * @property string $resume
 * @property string $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 */
class Users extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'image',
        'resume',
        'email_verified_at',
        'password',
        'remember_token',
        'created_at',
        'updated_at'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property boolean $status
 * @property string $description
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'category';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'status',
        'description',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

}

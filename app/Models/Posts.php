<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $description
 * @property string $resume
 * @property boolean $status
 * @property string $image_main
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class Posts extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'resume',
        'status',
        'image_main',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
}

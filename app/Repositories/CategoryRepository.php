<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Facades\DB;

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Category::class;
}

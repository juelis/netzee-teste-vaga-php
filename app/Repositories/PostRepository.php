<?php

namespace App\Repositories;

use App\Models\Posts;
use Illuminate\Support\Facades\DB;

/**
 * Class PostRepository.
 */
class PostRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Posts::class;
}

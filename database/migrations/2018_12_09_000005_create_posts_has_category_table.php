<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsHasCategoryTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'posts_has_category';

    /**
     * Run the migrations.
     * @table posts_has_category
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('posts_id');
            $table->integer('category_id');

            $table->index(["posts_id"], 'fk_posts_has_category_posts_idx');

            $table->index(["category_id"], 'fk_posts_has_category_category1_idx');


            $table->foreign('posts_id', 'fk_posts_has_category_posts_idx')
                ->references('id')->on('posts')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('category_id', 'fk_posts_has_category_category1_idx')
                ->references('id')->on('category')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}

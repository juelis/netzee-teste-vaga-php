<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('post', 'PostController@index')->name('getpost');
Route::get('post/create', 'PostController@create')->name('createpost');
Route::post('post/store', 'PostController@store')->name('storepost');
Route::get('post/show', 'PostController@show')->name('showpost');
Route::get('post/edit', 'PostController@edit')->name('editpost');
Route::update('post/update', 'PostController@update')->name('updatepost');
Route::delete('post/destroy', 'PostController@destroy')->name('destroypost');

<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('category', 'CategoryController@index')->name('getcategory');
Route::get('category/create', 'CategoryController@create')->name('createcategory');
Route::post('category/store', 'CategoryController@store')->name('storecategory');
Route::get('category/show', 'CategoryController@show')->name('showcategory');
Route::get('category/edit', 'CategoryController@edit')->name('editcategory');
Route::update('category/update', 'CategoryController@update')
    ->name('updatecategory');
Route::delete('category/destroy', 'CategoryController@destroy')
    ->name('destroycategory');
